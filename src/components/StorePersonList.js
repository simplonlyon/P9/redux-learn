import { connect } from "react-redux";
import PersonList from "./PersonList";
import { asyncRemovePerson } from "../store/person/actions";

const mapStateToProps = state => ({
    data: state.persons
});

const StorePersonList = connect(mapStateToProps, {removePerson:asyncRemovePerson})(PersonList);
export default StorePersonList;