import React from 'react';
import withToggle from '../hoc/WithToggle';


function SeeMore({display, toggle}) {

    const text = `L'histoire de toute société jusqu'à nos jours n'a été que l'histoire de luttes de classes.
    Homme libre et esclave, patricien et plébéien, baron et serf, maître de jurande et compagnon, en un mot oppresseurs et opprimés, en opposition constante, ont mené une guerre ininterrompue, tantôt ouverte, tantôt dissimulée, une guerre qui finissait toujours soit par une transformation révolutionnaire de la société tout entière, soit par la destruction des deux classes en lutte.
    Dans les premières époques historiques, nous constatons presque partout une organisation complète de la société en classes distinctes, une échelle graduée de conditions sociales. Dans la Rome antique, nous trouvons des patriciens, des chevaliers, des plébéiens, des esclaves; au moyen âge, des seigneurs, des vassaux, des maîtres de corporation, des compagnons, des serfs et, de plus, dans chacune de ces classes, une hiérarchie particulière.`;


    return (
        <p>
            
            {display && text}
            {!display && text.substr(0, 40)+'...'}
            <button onClick={toggle}>See More</button>
        </p>
    );
}

export const ToggleSeeMore = withToggle(SeeMore);