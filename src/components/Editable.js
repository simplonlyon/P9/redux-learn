import React from "react";
import withToggle from "../hoc/WithToggle";
import { connect } from "react-redux";
import { changeAppTitle } from "../store/actions";

function Editable({display, toggle, text, changeAppTitle}) {
    
    const handleChange = event => {
        changeAppTitle(event.target.value)
    };

    return (
        <>
            {!display ?
            (<p onDoubleClick={toggle}>{text}</p>):
            (<input onBlur={toggle} onChange={handleChange} type="text" value={text} />)}
        </>
    );
}

const mapStateToProps = state => ({
    text: state.first.appTitle
});

export const ToggleEditable = connect(mapStateToProps, {changeAppTitle})(withToggle(Editable));