import React from "react";
import withLoading from "../hoc/WithLoading";

export default function CountPerson({data}) {
    const count = data.length;

    return (
        <p>{count}</p>
    );
}

export const LoadingCountPerson = withLoading(CountPerson);