import React from "react";
import { connect } from "react-redux";
import { login, logout } from "../store/actions";

const mapStateToProps = state => ({
    logged: state.first.logged
});

function ConnectedLogin({logged, login, logout}) {
    
    return (
        <>
            {!logged ? 
            <button onClick={login}>Login</button>:
            <button onClick={logout}>Logout</button>}
        </>
    )
}

const Login = connect(mapStateToProps, {login, logout})(ConnectedLogin);

export default Login;
