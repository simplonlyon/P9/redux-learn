import React from "react";
import withLoading from "../hoc/WithLoading";



export default function PersonList({ data, removePerson }) {

    return (
        <ul>
            {data.map(person => <li key={person.id}>{person.name} <button onClick={() => removePerson(person)}>X</button></li>)}
        </ul>
    );
}

export const LoadingPersonList = withLoading(PersonList);