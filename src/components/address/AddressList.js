import React from 'react'
import { connect } from 'react-redux';
import Address from './Address';


function ConnectedAddressList({addresses}) {
    return (
        <ul>
            {addresses.map(address => 
            <Address key={address.id} address={address} />)}
        </ul>
    )
}

const mapStateToProps = (state) => ({
    addresses:state.address   
});

const AddressList = connect(mapStateToProps)(ConnectedAddressList);
export default AddressList;
