import React from 'react'

export default function Address({address}) {
    return (
    <li>{address.number}, {address.street}, {address.zipCode}</li>
    )
}
