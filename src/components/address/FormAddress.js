import React from 'react'
import { Field, reduxForm } from 'redux-form'
import RenderField from '../RenderField';
import { validateAddress, asyncValidateAddress } from '../../services/address-validation';

function FormAddress(props) {
    console.log(props);
    let {handleSubmit} = props;
    return (
        <form onSubmit={handleSubmit}>
            <Field component={RenderField} label="Number :"  name="number" />
            
            <Field component={RenderField} label="Street :"  name="street" />

            <Field component={RenderField} label="Zip Code :" name="zipCode" />
            
            <button>Submit</button>
        </form>
    )
}

export default FormAddress = reduxForm({
    form: 'address',
    validate: validateAddress,
    // asyncValidate: asyncValidateAddress
})(FormAddress);
