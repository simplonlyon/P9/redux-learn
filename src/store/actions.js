import { CONNECT, DISCONNECT, CHANGE_APP_TITLE } from "./constants";



export function login() {
    return {
        type: CONNECT
    }
}

export function logout() {
    return {
        type: DISCONNECT
    }
}

/**
 * @param {string} title the new app title
 */
export function changeAppTitle(title) {
    return {
        payload: title,
        type: CHANGE_APP_TITLE
    }
}