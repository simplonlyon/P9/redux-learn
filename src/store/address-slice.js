import { createSlice } from "@reduxjs/toolkit";
import { getAddresses, postAddress } from "../services/address-service";

const addresSlice = createSlice({
    name: "address",
    initialState: [],
    reducers: {
        addAddress(state, {payload}) {
            state.push(payload);
            return state;
        },
        removeAddress(state, action) {
            return state.filter(item => item.id !== action.payload);
        },
        changeAddressList(state, action) {
            return action.payload;
        }
    }
});

export const addressReducer = addresSlice.reducer;

export const {addAddress, changeAddressList, removeAddress} = addresSlice.actions;

export const asyncGetAddresses = () => async dispatch => {
    const addresses = await getAddresses();
    return dispatch(changeAddressList(addresses));
}

export const asyncAddAddress = (address) => async dispatch => {
    const added = await postAddress(address);
    return dispatch(addAddress(added));
}
