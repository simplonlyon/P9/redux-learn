import { CONNECT, DISCONNECT, CHANGE_APP_TITLE } from "./constants";

const initialState = {
    logged: false,
    appTitle: 'My Application Title'
}

export function firstReducer(state = initialState, action) {

    switch(action.type) {
        case CONNECT:
            // return Object.assign({}, state, {logged: true});
            return {
                ...state,
                logged: true
            }
        case DISCONNECT:
            return {
                ...state,
                logged: false
            }
        case CHANGE_APP_TITLE:
            return {
                ...state,
                appTitle: action.payload
            }
        default:
            return state;
        
    }
}
