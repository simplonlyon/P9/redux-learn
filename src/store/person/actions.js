import { ADD_PERSON, REMOVE_PERSON, CHANGE_PERSON_LIST } from "../constants";
import { findAll, remove } from "../../services/person-service";

//SYNCHRONE

export function addPerson(person) {
    return {
        type: ADD_PERSON,
        payload: person
    }
}

export const removePerson = (person) => {
    return {
        type: REMOVE_PERSON,
        payload: person
    }
}

export const changePersonList = (list) => {
    return {
        type: CHANGE_PERSON_LIST,
        payload: list
    }
}

// ASYNCHRONE

export const fetchPerson = () => async dispatch => {
    const persons = await findAll();
    return dispatch(changePersonList(persons));
}

export const asyncRemovePerson = person =>  async dispatch => {
    await remove(person.id);
    return dispatch(removePerson(person));
}

