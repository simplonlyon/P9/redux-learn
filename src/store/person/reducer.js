import { ADD_PERSON, REMOVE_PERSON, CHANGE_PERSON_LIST } from "../constants";

const initialState = [
    {id:1, name:'Jean'},   
    {id:2, name:'Charlie'},   
    {id:3, name:'Anna'}   
];

/**
 * @param {{type:string, payload:any}} action 
 */
export function personReducer(state = initialState, action) {

    switch (action.type) {
        case ADD_PERSON:
            return [
                ...state,
                action.payload
            ];
        case REMOVE_PERSON:
            return state.filter(item => item.id !== action.payload.id);
        case CHANGE_PERSON_LIST:
            return action.payload;
        default:
            return state;
    }
}