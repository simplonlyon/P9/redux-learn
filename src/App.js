import React from 'react';

import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { FirstHOC } from './pages/FirstHOC';
import Loading from './pages/Loading';
import Authentication from './pages/Authentication';
import { useSelector } from 'react-redux';
import PersonPage from './pages/PersonPage';
import AddressPage from './pages/AddressPage';

function App() {
  const title = useSelector(state => state.first.appTitle);
  return (
    <BrowserRouter>
      <div className="App">
        <h1>{title}</h1>
        <Switch>
          <Route exact path="/" component={FirstHOC} />
          <Route path="/loading" component={Loading} />
          <Route path="/auth" component={Authentication} />
          <Route path="/person" component={PersonPage} />
          <Route path="/address" component={AddressPage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
