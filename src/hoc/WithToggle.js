import React from "react";

export default function withToggle(WrappedComponent) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                display: false
            }
        }

        toggle() {
            if(this.state.display) {
                this.setState({display: false});
            } else {
                this.setState({display: true});
            }
        }

        render() {
            return <WrappedComponent 
                    display={this.state.display} 
                    toggle={() => this.toggle()} 
                    {...this.props}/>
        }
    }
}