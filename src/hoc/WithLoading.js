import React from "react";
import { findAll } from "../services/person-service";
import Loader from "../components/Loader";

export default function withLoading(WrappedComponent) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                loading: true,
                data: []
            };
        }

        componentDidMount() {
            setTimeout(() =>
                findAll()
                .then(data => this.setState({
                    data,
                    loading: false
                }))
            , 2000);
        }

        render() {
            return (
                <>
                {this.state.loading ?
                    <Loader />:
                    <WrappedComponent data={this.state.data} {...this.props}/>
                }
                </>
            );
        }
    }
}