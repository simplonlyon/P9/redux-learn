import React from "react";
import Login from "../components/Login";
import { useSelector } from "react-redux";

export default function Authentication() {
    const logged = useSelector(state => state.first.logged);

    return (
        <>
            {logged && <p>You are connected</p>}
            <Login />
        </>
    );
}