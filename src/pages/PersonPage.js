import React, { useEffect } from "react";
import StorePersonList from "../components/StorePersonList";
import { fetchPerson } from "../store/person/actions";
import { useDispatch } from "react-redux";


export default function PersonPage() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPerson());
    }, [dispatch]);

    return (
        <>
            <StorePersonList />
        </>
    );
}