import React from "react";
import { ToggleSeeMore } from "../components/SeeMore";
import { ToggleEditable } from "../components/Editable";

export function FirstHOC() {

    return (
        <>
            <p>Page FirstHOC</p>

            <ToggleSeeMore />
            <ToggleEditable />
        </>
    );
}