import React from "react";
import {LoadingPersonList} from "../components/PersonList";
import {LoadingCountPerson} from "../components/CountPerson";

export default function Loading() {
    return (
        <>
            <LoadingPersonList />
            <LoadingCountPerson />
        </>
    )
}