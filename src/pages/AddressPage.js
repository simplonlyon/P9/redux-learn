import React, { useEffect } from 'react'
import AddressList from '../components/address/AddressList'
import { useDispatch } from 'react-redux'
import { asyncGetAddresses, asyncAddAddress } from '../store/address-slice';
import FormAddress from '../components/address/FormAddress';
import { reset } from 'redux-form';

export default function AddressPage() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(asyncGetAddresses());
    }, [dispatch]); 
    
    const addAddress = (address) => {
        dispatch(asyncAddAddress(address))
        .then(() => dispatch(reset('address')));
    }
    

    return (
        <>
            <AddressList />
            <FormAddress onSubmit={addAddress} />
        </>
    )
}


