

export async function findAll() {
    const response = await fetch('http://localhost:4000/person');
    const data = await response.json();
    return data;
}

export async function remove(id) {
    return await fetch('http://localhost:4000/person/'+id, {
        method: 'DELETE'
    });
}