export const validateAddress = values => {
    const errors = {};
    if (!values.number) {
      errors.number = 'Required';
    } else if (!/^[0-9]+( ?B(is)?| ?T(er)?)?$/i.test(values.number)) {
      errors.number = 'The address must only contains numbers and Bis/Ter';
    }
    if (!values.street) {
      errors.street = 'Required'
    }
    if (!values.zipCode) {
      errors.zipCode = 'Required'
    } 
    return errors
  }
  
  export const asyncValidateAddress = async (values /*, dispatch */) => {
      let query = `${values.number} ${values.street} ${values.zipCode}`;
        let response = await fetch('https://api-adresse.data.gouv.fr/search/?q='+query);
        let data = await response.json();
        if(data.features.length < 1 || data.features[0].properties.score < 0.6) {
            throw {form: 'This address does not exist'};
        }
        return data;
  }