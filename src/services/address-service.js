

export async function getAddresses() {
    const response = await fetch('http://localhost:4000/address');
    const data = await response.json();
    return data;
}

export async function deleteAddress(id) {
    return await fetch('http://localhost:4000/address/'+id, {
        method: 'DELETE'
    });
}

export async function postAddress(address) {
    
    let response =  await fetch('http://localhost:4000/address', {
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(address)
    });
    let data = await response.json();
    return data;
}